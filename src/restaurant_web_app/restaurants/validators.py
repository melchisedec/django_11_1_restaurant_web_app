from django.core.exceptions import ValidationError

def validate_even( value ):
	if value % 2 != 0:
		raise ValidationError(
			'%(value)s is not an even number',
		params = { 'value': vlue },
		)


def validate_email( value ):
	email = value
	if '.edu' in email:
		raise forms.ValidationError( 'We dont accept edu emails' )


CATEGORIES = [ 'African', 'Exotics', 'Spices', 'Aob' ]

def validate_category( value ):
	cat = value.capitalize()
	if not value in CATEGORIES and not cat in CATEGORIES:
		raise ValidationError( f"{value} not a valid category" )
	return cat