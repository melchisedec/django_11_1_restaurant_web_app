from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
import random
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView, CreateView, UpdateView

from .forms import RestaurantCreateForm, RestaurantLocationCreateForm
from .models import RestaurantLocation



class RestaurantListView( LoginRequiredMixin, ListView ):
	#template_name = 'restaurants/restaurants_list.html'
	def get_queryset( self ):
		return RestaurantLocation.objects.filter( owner=self.request.user )


class RestaurantDetailView( LoginRequiredMixin, DetailView ):
	#template_name = 'restaurants/restaurants_list.html'
	def get_queryset( self ):
		return RestaurantLocation.objects.filter( owner=self.request.user )

	# def get_context_data( self, *args, **kwargs ):
	# 	print( self.kwargs )
	# 	context = super( RestaurantDetailView, self ). get_context_data( *args, **kwargs )
	# 	print( context )
	# 	return context

	# def get_object( self, *args, **kwargs ):
	# 	rest_id = self.kwargs.get( 'rest_id' )
	# 	obj = get_object_or_404( RestaurantLocation, id=rest_id )
	# 	return obj

class RestaurantCreateView( LoginRequiredMixin, CreateView ):
	form_class = RestaurantLocationCreateForm
	login_url = '/login/'
	template_name = 'restaurants/form.html'
	#success_url = '/restaurants/'

	def form_valid( self, form ):
		instance = form.save( commit=False )
		instance.owner = self.request.user
		#instance.save() # because form_valid is === form.save()
		return super( RestaurantCreateView, self ).form_valid( form )

	def get_context_data( self, *args, **kwargs ):
		context = super( RestaurantCreateView, self ).get_context_data( *args, **kwargs )
		context[ 'title' ] = 'Add Restaurant'
		return context

class RestaurantUpdateView( LoginRequiredMixin, UpdateView ):
	form_class = RestaurantLocationCreateForm
	login_url = '/login/'
	#template_name = 'restaurants/form.html'
	template_name = 'restaurants/detail-update.html'
	#success_url = '/restaurants/'

	def form_valid( self, form ):
		instance = form.save( commit=False )
		instance.owner = self.request.user
		#instance.save() # because form_valid is === form.save()
		return super( RestaurantUpdateView, self ).form_valid( form )

	def get_context_data( self, *args, **kwargs ):
		context = super( RestaurantUpdateView, self ).get_context_data( *args, **kwargs )
		name = self.get_object().name
		context[ 'title' ] = f'Update Restaurant: {name}'
		return context

	def get_queryset( self ):
		return RestaurantLocation.objects.filter( owner=self.request.user )


"""
class RestaurantListView( ListView ):
	template_name = 'restaurants/restaurants_list.html'
	def get_queryset( self ):
		#print( self.kwargs )
		slug = self.kwargs.get( 'slug' )
		if slug:
			queryset = RestaurantLocation.objects.filter( 
				Q( category__iexact= slug ) | Q( category__icontains= slug )
			)
		else:
			queryset = RestaurantLocation.objects.all()
		return queryset
"""

"""
@login_required( login_url = '/login/' )
def restaurant_createview( request ):
	form = RestaurantLocationCreateForm( request.POST or None )
	errors = None
	if form.is_valid():
		if request.user.is_authenticated():
			instance = form.save( commit=False )
			instance.owner = request.user
			instance.save()
			return HttpResponseRedirect( '/restaurants/' )
		else:
			return HttpResponseRedirect( '/login' )
	if form.errors:
		errors = form.errors

	template_name = 'restaurants/form.html'
	context = {
		"form" : form, "errors" : errors
	}
	return render( request, template_name, context)

"""
#function based view
"""
def restaurant_createview( request ):
	form = RestaurantCreateForm( request.POST or None )
	errors = None
	#print( request.POST )
	#if request.method == 'POST':
		# print( 'Post data' )
		# print( request.POST )
		# title = request.POST.get( 'title' )
		# location = request.POST.get( 'location' )
		# category = request.POST.get( 'category' )
		#form = RestaurantCreateForm( request.POST )
	if form.is_valid():
		obj = RestaurantLocation.objects.create(
				name = form.cleaned_data.get( 'name' ),
				location = form.cleaned_data.get( 'location' ),
				category = form.cleaned_data.get( 'category' ),
			)
		return HttpResponseRedirect( '/restaurants/' )
	if form.errors:
		errors = form.errors
		#print( form.errors )

		
	template_name = 'restaurants/form.html'
	#queryset = RestaurantLocation.objects.all()
	context = {
		"form" : form, "errors" : errors
	}
	return render( request, template_name, context)
"""
"""
def restaurant_listview( request ):
	template_name = 'restaurants/restaurants_list.html'
	queryset = RestaurantLocation.objects.all()
	context = {
		"object_list" : queryset
	}
	return render( request, template_name, context)
"""

# class SearchRestaurantListView( ListView ):	
# 	template_name = 'restaurants/restaurants_list.html'

# 	def get_queryset( self ):
# 		#print( self.kwargs )
# 		slug = self.kwargs.get( 'slug' )
# 		if slug:
# 			queryset = RestaurantLocation.objects.filter( 
# 				Q( category__iexact= slug ) | Q( category__icontains= slug )
# 			)
# 		else:
# 			queryset = RestaurantLocation.objects.none()
# 		return queryset


# class SpicesRestaurantListView( ListView ):
# 	queryset = RestaurantLocation.objects.filter( category__iexact='spices' )
# 	template_name = 'restaurants/restaurants_list.html'

# class AfricanDishRestaurantListView( ListView ):
# 	queryset = RestaurantLocation.objects.filter( category__iexact='African dish' )
# 	template_name = 'restaurants/restaurants_list.html'


# class HomeView( TemplateView ):
# 	template_name = 'home.html'

# 	def get_context_data( self, *args, **kwargs ):
# 		context = super( HomeView, self ).get_context_data(  *args, **kwargs )
# 		num = random.randint( 0, 10000000 )
# 		some_list = [ num, random.randint( 2,2000 ), random.randint( 509, 24566 ) ]
# 		condition_bool_item = True
# 		context = { 
# 					"html_var" : False, 
# 					"num" : num,
# 					"some_list" : some_list
# 		}
# 		#print( context )
# 		return context

# class AboutView( TemplateView ):
# 	template_name = 'about.html'

# class ContactView( TemplateView ):
# 	template_name = 'contact.html'


# def home( request ):

# 	num = random.randint( 0, 10000000 )
# 	template = 'home.html'
# 	some_list = [ num, random.randint( 2,2000 ), random.randint( 509, 24566 ) ]
# 	condition_bool_item = True
# 	context = { 
# 				"html_var" : False, 
# 				"num" : num,
# 				"some_list" : some_list
# 	}
# 	return render( request, template, context )

# def about( request ):
# 	template = 'about.html'
# 	context = { 
# 				}
# 	return render( request, template, context )

# def contact( request ):
# 	template = 'contact.html'
# 	context = { 
# 				}
# 	return render( request, template, context )

# class ContactView( View ):
# 	def get( slef, request, *args, **kwargs ):
# 		#print( kwargs )
# 		template = 'contact.html'
# 		context = { 
# 				}
# 		return render( request, template, context )




# Create your views here.
#function based view
# def home( request ):
# 	html_var = 'f strings showing'
# 	html_ = f"""
# 	<!DOCTYPE html>
# <html>
# <head>
# 	<title></title>
# </head>
# <body>
# 	<h1>Testing One two</h1>
# 	<p>This {html_var} is coming up</p>
# </body>
# </html>
# 	"""
	#return HttpResponse( 'HELLO THERE' )
	#return HttpResponse( html_ )
	#return render( request, template, context )










